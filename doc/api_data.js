define({ "api": [
  {
    "type": "put",
    "url": "/api/auth/",
    "title": "Update auth module",
    "name": "Get_other",
    "group": "Authentication",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  \"Authorization\": \"Bearer JWT\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>User's password</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User's email</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  \"email\": \"john@doe.com\",\n  \"password\": \"test123456\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  \"success\": 1,\n  \"data\": {\n    \"auth\": [Object]\n  },\n  \"message\": \"auth updated successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/auth/controller.js",
    "groupTitle": "Authentication"
  },
  {
    "type": "post",
    "url": "/api/auth/local",
    "title": "Local Signup and Login",
    "name": "Local_Authentication",
    "group": "Authentication",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>User's full name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>User's username</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>User's password</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User's email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "accType",
            "defaultValue": "user",
            "description": "<p>User's account type</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": true,
            "field": "signp",
            "defaultValue": "false",
            "description": "<p>To toggle between signup and login</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Signup",
          "content": "{\n  \"name\": \"John Doe\",\n  \"username\": \"johndoe\",\n  \"password\": \"test1234\",\n  \"email\": \"john@doe.com\",\n  \"accType\": \"admin\",\n  \"signup\": true\n}",
          "type": "json"
        },
        {
          "title": "Login",
          "content": "{\n  \"username\": \"johndoe\", //Username or email as value but key should be \"username\"\n  \"password\": \"test1234\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  \"success\": 1,\n  \"data\": {\n    \"token\": \"Bearer JWT token\"\n  },\n  \"message\": \"Loggedin successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 409 Record conflict",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/auth/controller.js",
    "groupTitle": "Authentication"
  },
  {
    "type": "post",
    "url": "/api/auth/social",
    "title": "Social Signup and Login",
    "name": "Social_Authentication",
    "group": "Authentication",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>User's full name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>User's username</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User's email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "scId",
            "description": "<p>Social network ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "scToken",
            "description": "<p>Social network token</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "scType",
            "description": "<p>Name of the social network</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  \"name\": \"John Doe\",\n  \"username\": \"johndoe\",\n  \"email\": \"john@doe.com\",\n  \"scId\": \"123456789\",\n  \"scToken\": \"123456789\",\n  \"scType\": \"facebook\" // Or twitter / google\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  \"success\": 1,\n  \"data\": {\n    \"token\": \"Bearer JWT token\"\n  },\n  \"message\": \"Loggedin successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 409 Record conflict",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/auth/controller.js",
    "groupTitle": "Authentication"
  },
  {
    "type": "post",
    "url": "/api/coupon",
    "title": "Create coupon",
    "name": "Create_coupon",
    "group": "Coupon",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>Coupon code</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "membershipType",
            "description": "<p>Membership type</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "couponValue",
            "description": "<p>Coupon value</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "validFrom",
            "description": "<p>Coupon code</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "validTo",
            "description": "<p>Coupon code</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "desc",
            "description": "<p>Coupon description</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    coupon: [Object]\n  },\n  message: coupon created successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/coupon/controller.js",
    "groupTitle": "Coupon"
  },
  {
    "type": "delete",
    "url": "/api/coupon/:id",
    "title": "Delete coupon",
    "name": "Delete_coupon",
    "group": "Coupon",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    coupon: [Object]\n  },\n  message: coupon deleted successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/coupon/controller.js",
    "groupTitle": "Coupon"
  },
  {
    "type": "get",
    "url": "/api/coupon/:member",
    "title": "Get all coupon",
    "name": "Get_all_coupon",
    "group": "Coupon",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    coupons: [Object]\n  },\n  message: coupon details fetched successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/coupon/controller.js",
    "groupTitle": "Coupon"
  },
  {
    "type": "put",
    "url": "/api/coupon/:id",
    "title": "Update coupon",
    "name": "Update_coupon",
    "group": "Coupon",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "ExampleFieldName",
            "description": "<p>Example details</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  ExampleFieldName: Example Value\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    coupon: [Object]\n  },\n  message: coupon updated successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/coupon/controller.js",
    "groupTitle": "Coupon"
  },
  {
    "type": "post",
    "url": "/api/fitnessgoal",
    "title": "Create fitnessgoal",
    "name": "Create_fitnessgoal",
    "group": "Fitnessgoal",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "goals",
            "description": "<p>Fitness goal titles</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  \"goals\": \"Weight gain\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    fitnessgoal: [Object]\n  },\n  message: fitnessgoal created successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/fitnessgoal/controller.js",
    "groupTitle": "Fitnessgoal"
  },
  {
    "type": "delete",
    "url": "/api/fitnessgoal/:id",
    "title": "Delete fitnessgoal",
    "name": "Delete_fitnessgoal",
    "group": "Fitnessgoal",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    fitnessgoal: [Object]\n  },\n  message: fitnessgoal deleted successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/fitnessgoal/controller.js",
    "groupTitle": "Fitnessgoal"
  },
  {
    "type": "get",
    "url": "/api/fitnessgoal",
    "title": "Get all fitnessgoal",
    "name": "Get_all_fitnessgoal",
    "group": "Fitnessgoal",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    fitnessgoals: [Object]\n  },\n  message: fitnessgoal details fetched successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/fitnessgoal/controller.js",
    "groupTitle": "Fitnessgoal"
  },
  {
    "type": "get",
    "url": "/api/fitnessgoal/:id",
    "title": "Get single fitnessgoal",
    "name": "Get_single_fitnessgoal",
    "group": "Fitnessgoal",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    fitnessgoal: [Object]\n  },\n  message: fitnessgoal details fetched successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/fitnessgoal/controller.js",
    "groupTitle": "Fitnessgoal"
  },
  {
    "type": "put",
    "url": "/api/fitnessgoal/:id",
    "title": "Update fitnessgoal",
    "name": "Update_fitnessgoal",
    "group": "Fitnessgoal",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "ExampleFieldName",
            "description": "<p>Example details</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  ExampleFieldName: Example Value\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    fitnessgoal: [Object]\n  },\n  message: fitnessgoal updated successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/fitnessgoal/controller.js",
    "groupTitle": "Fitnessgoal"
  },
  {
    "type": "get",
    "url": "/api/lead",
    "title": "Get all leads",
    "name": "All_Leads",
    "group": "Lead",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  \"Authorization\": \"Bearer JWT\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  \"success\": 1,\n  \"data\": {\n    \"leads\": [Object]\n  },\n  \"message\": \"leads fetched successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/lead/controller.js",
    "groupTitle": "Lead"
  },
  {
    "type": "post",
    "url": "/api/lead",
    "title": "Create lead",
    "name": "Create_lead",
    "group": "Lead",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "ObjectId",
            "optional": false,
            "field": "dp",
            "description": "<p>File ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>phone number of the lead</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>email address of the lead</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>full name of the lead</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "gender",
            "description": "<p>gender of the lead</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "source",
            "description": "<p>source type of the lead</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  \"phone\": \"9804313438\",\n  \"dp\": [ObjectId],\n  \"email\": \"xyz@xyz.com\",\n  \"name\": \"Smit Ray\",\n  \"gender\": \"male\",\n  \"source\": \"Walk in\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  \"success\": 1,\n  \"data\": {\n    \"token\": \"Bearer JWT token\"\n  },\n  \"message\": \"lead created successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 409 Record conflict",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/lead/controller.js",
    "groupTitle": "Lead"
  },
  {
    "type": "get",
    "url": "/api/lead/:id",
    "title": "Get single lead",
    "name": "Get_single_lead",
    "group": "Lead",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  \"Authorization\": \"Bearer JWT\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  \"success\": 1,\n  \"data\": {\n    \"lead\": [Object]\n  },\n  \"message\": \"lead fetched successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/lead/controller.js",
    "groupTitle": "Lead"
  },
  {
    "type": "put",
    "url": "/api/lead",
    "title": "Update lead",
    "name": "Update_lead",
    "group": "Lead",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  \"Authorization\": \"Bearer JWT\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  \"success\": 1,\n  \"data\": {\n    \"auth\": [Object]\n  },\n  \"message\": \"lead updated successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/lead/controller.js",
    "groupTitle": "Lead"
  },
  {
    "type": "post",
    "url": "/api/leadsource",
    "title": "Create leadsource",
    "name": "Create_leadsource",
    "group": "Leadsource",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "ExampleFieldName",
            "description": "<p>Example details</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  ExampleFieldName: Example Value\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    leadsource: [Object]\n  },\n  message: leadsource created successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/leadsource/controller.js",
    "groupTitle": "Leadsource"
  },
  {
    "type": "post",
    "url": "/api/leadsource/:id",
    "title": "Delete leadsource",
    "name": "Delete_leadsource",
    "group": "Leadsource",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    leadsource: [Object]\n  },\n  message: leadsource deleted successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/leadsource/controller.js",
    "groupTitle": "Leadsource"
  },
  {
    "type": "get",
    "url": "/api/leadsource",
    "title": "Get all leadsource",
    "name": "Get_all_leadsource",
    "group": "Leadsource",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    leadsources: [Object]\n  },\n  message: leadsource details fetched successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/leadsource/controller.js",
    "groupTitle": "Leadsource"
  },
  {
    "type": "get",
    "url": "/api/leadsource/:id",
    "title": "Get single leadsource",
    "name": "Get_single_leadsource",
    "group": "Leadsource",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    leadsource: [Object]\n  },\n  message: leadsource details fetched successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/leadsource/controller.js",
    "groupTitle": "Leadsource"
  },
  {
    "type": "post",
    "url": "/api/leadsource/:id",
    "title": "Update leadsource",
    "name": "Update_leadsource",
    "group": "Leadsource",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "ExampleFieldName",
            "description": "<p>Example details</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  ExampleFieldName: Example Value\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    leadsource: [Object]\n  },\n  message: leadsource updated successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/leadsource/controller.js",
    "groupTitle": "Leadsource"
  },
  {
    "type": "get",
    "url": "/api/member",
    "title": "Get all members",
    "name": "All_members",
    "group": "Member",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  \"Authorization\": \"Bearer JWT\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  \"success\": 1,\n  \"data\": {\n    \"members\": [Object]\n  },\n  \"message\": \"members fetched successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/auth/controller.js",
    "groupTitle": "Member"
  },
  {
    "type": "post",
    "url": "/api/member",
    "title": "Create member",
    "name": "Create_member",
    "group": "Member",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  \"success\": 1,\n  \"data\": {\n    \"token\": \"Bearer JWT token\"\n  },\n  \"message\": \"members created successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 409 Record conflict",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/auth/controller.js",
    "groupTitle": "Member"
  },
  {
    "type": "get",
    "url": "/api/member/:id",
    "title": "Get single member",
    "name": "Get_single_member",
    "group": "Member",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  \"Authorization\": \"Bearer JWT\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  \"success\": 1,\n  \"data\": {\n    \"member\": [Object]\n  },\n  \"message\": \"member fetched successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/auth/controller.js",
    "groupTitle": "Member"
  },
  {
    "type": "post",
    "url": "/api/membershiptype",
    "title": "Create membershiptype",
    "name": "Create_membershiptype",
    "group": "Membershiptype",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "memberType",
            "description": "<p>Membership type title</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  \"memberType\": \"Membership\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    membershiptype: [Object]\n  },\n  message: membershiptype created successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/membershiptype/controller.js",
    "groupTitle": "Membershiptype"
  },
  {
    "type": "delete",
    "url": "/api/membershiptype/:id",
    "title": "Delete membershiptype",
    "name": "Delete_membershiptype",
    "group": "Membershiptype",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    membershiptype: [Object]\n  },\n  message: membershiptype deleted successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/membershiptype/controller.js",
    "groupTitle": "Membershiptype"
  },
  {
    "type": "get",
    "url": "/api/membershiptype",
    "title": "Get all membershiptype",
    "name": "Get_all_membershiptype",
    "group": "Membershiptype",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    membershiptypes: [Object]\n  },\n  message: membershiptype details fetched successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/membershiptype/controller.js",
    "groupTitle": "Membershiptype"
  },
  {
    "type": "get",
    "url": "/api/membershiptype/:id",
    "title": "Get single membershiptype",
    "name": "Get_single_membershiptype",
    "group": "Membershiptype",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    membershiptype: [Object]\n  },\n  message: membershiptype details fetched successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/membershiptype/controller.js",
    "groupTitle": "Membershiptype"
  },
  {
    "type": "put",
    "url": "/api/membershiptype/:id",
    "title": "Update membershiptype",
    "name": "Update_membershiptype",
    "group": "Membershiptype",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "ExampleFieldName",
            "description": "<p>Example details</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  ExampleFieldName: Example Value\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    membershiptype: [Object]\n  },\n  message: membershiptype updated successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/membershiptype/controller.js",
    "groupTitle": "Membershiptype"
  },
  {
    "type": "post",
    "url": "/api/package",
    "title": "Create package",
    "name": "Create_package",
    "group": "Package",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "pkgName",
            "description": "<p>Package name</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "cost",
            "description": "<p>Package cost</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "duration",
            "description": "<p>Package duration in months</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "validityFrom",
            "description": "<p>Package validity from</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "validityTo",
            "description": "<p>Package validity to</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "activities",
            "description": "<p>Package activity</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "maxDiscount",
            "description": "<p>Package maximum discount</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "percentage",
            "description": "<p>Package percentage</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "freezable",
            "description": "<p>Package freezable</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "transferrable",
            "description": "<p>Package transferrable</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "desc",
            "description": "<p>Package description</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    package: [Object]\n  },\n  message: package created successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/package/controller.js",
    "groupTitle": "Package"
  },
  {
    "type": "delete",
    "url": "/api/package/:id",
    "title": "Delete package",
    "name": "Delete_package",
    "group": "Package",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    package: [Object]\n  },\n  message: package deleted successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/package/controller.js",
    "groupTitle": "Package"
  },
  {
    "type": "get",
    "url": "/api/package",
    "title": "Get all package",
    "name": "Get_all_package",
    "group": "Package",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    packages: [Object]\n  },\n  message: package details fetched successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/package/controller.js",
    "groupTitle": "Package"
  },
  {
    "type": "get",
    "url": "/api/package/:id",
    "title": "Get single package",
    "name": "Get_single_package",
    "group": "Package",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    package: [Object]\n  },\n  message: package details fetched successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/package/controller.js",
    "groupTitle": "Package"
  },
  {
    "type": "put",
    "url": "/api/package/:id",
    "title": "Update package",
    "name": "Update_package",
    "group": "Package",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "ExampleFieldName",
            "description": "<p>Example details</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  ExampleFieldName: Example Value\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    package: [Object]\n  },\n  message: package updated successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/package/controller.js",
    "groupTitle": "Package"
  },
  {
    "type": "post",
    "url": "/api/specialization",
    "title": "Create specialization",
    "name": "Create_specialization",
    "group": "Specialization",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "special",
            "description": "<p>Specialization title</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  \"special\": \"Aerobics\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    specialization: [Object]\n  },\n  message: specialization created successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/specialization/controller.js",
    "groupTitle": "Specialization"
  },
  {
    "type": "delete",
    "url": "/api/specialization/:id",
    "title": "Delete specialization",
    "name": "Delete_specialization",
    "group": "Specialization",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    specialization: [Object]\n  },\n  message: specialization deleted successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/specialization/controller.js",
    "groupTitle": "Specialization"
  },
  {
    "type": "get",
    "url": "/api/specialization",
    "title": "Get all specialization",
    "name": "Get_all_specialization",
    "group": "Specialization",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    specializations: [Object]\n  },\n  message: specialization details fetched successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/specialization/controller.js",
    "groupTitle": "Specialization"
  },
  {
    "type": "get",
    "url": "/api/specialization/:id",
    "title": "Get single specialization",
    "name": "Get_single_specialization",
    "group": "Specialization",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    specialization: [Object]\n  },\n  message: specialization details fetched successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/specialization/controller.js",
    "groupTitle": "Specialization"
  },
  {
    "type": "put",
    "url": "/api/specialization/:id",
    "title": "Update specialization",
    "name": "Update_specialization",
    "group": "Specialization",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "ExampleFieldName",
            "description": "<p>Example details</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  ExampleFieldName: Example Value\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    specialization: [Object]\n  },\n  message: specialization updated successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/specialization/controller.js",
    "groupTitle": "Specialization"
  },
  {
    "type": "post",
    "url": "/api/staffroles",
    "title": "Create staffroles",
    "name": "Create_staffroles",
    "group": "Staffroles",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "roles",
            "description": "<p>Staff roles</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  \"roles\": \"Admin\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    staffroles: [Object]\n  },\n  message: staffroles created successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/staffroles/controller.js",
    "groupTitle": "Staffroles"
  },
  {
    "type": "delete",
    "url": "/api/staffroles/:id",
    "title": "Delete staffroles",
    "name": "Delete_staffroles",
    "group": "Staffroles",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    staffroles: [Object]\n  },\n  message: staffroles deleted successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/staffroles/controller.js",
    "groupTitle": "Staffroles"
  },
  {
    "type": "get",
    "url": "/api/staffroles",
    "title": "Get all staffroles",
    "name": "Get_all_staffroles",
    "group": "Staffroles",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    staffroless: [Object]\n  },\n  message: staffroles details fetched successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/staffroles/controller.js",
    "groupTitle": "Staffroles"
  },
  {
    "type": "get",
    "url": "/api/staffroles/:id",
    "title": "Get single staffroles",
    "name": "Get_single_staffroles",
    "group": "Staffroles",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    staffroles: [Object]\n  },\n  message: staffroles details fetched successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/staffroles/controller.js",
    "groupTitle": "Staffroles"
  },
  {
    "type": "put",
    "url": "/api/staffroles/:id",
    "title": "Update staffroles",
    "name": "Update_staffroles",
    "group": "Staffroles",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  Authorization: Bearer JWT\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "ExampleFieldName",
            "description": "<p>Example details</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  ExampleFieldName: Example Value\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  success: 1,\n  data: {\n    staffroles: [Object]\n  },\n  message: staffroles updated successfully\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/staffroles/controller.js",
    "groupTitle": "Staffroles"
  },
  {
    "type": "get",
    "url": "/api/auth",
    "title": "Get my details",
    "name": "Get_me",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  \"Authorization\": \"Bearer JWT\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  \"success\": 1,\n  \"data\": {\n    \"user\": [Object]\n  },\n  \"message\": \"User details fetched successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/auth/controller.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/api/auth/:id",
    "title": "Get other's details",
    "name": "Get_other",
    "group": "User",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  \"Authorization\": \"Bearer JWT\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  \"success\": 1,\n  \"data\": {\n    \"user\": [Object]\n  },\n  \"message\": \"User details fetched successfully\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Email or Username exists",
          "content": "HTTP/1.1 404 No record found",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/auth/controller.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/api/files/delete",
    "title": "File delete",
    "name": "Delete_file",
    "group": "Utility",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  \"Authorization\": \"Bearer JWT\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "fileId",
            "description": "<p>File / Files (Use form data)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  \"fileId\": \"file\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  \"success\": 1,\n  \"data\": {\n    \"files\": {\n      \"filename\": \"file name\",\n      \"permalink\": \"link of the file\",\n      \"_id\": \"file ID\"\n    }\n  },\n  \"message\": \"File deleted\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/files/controller.js",
    "groupTitle": "Utility"
  },
  {
    "type": "post",
    "url": "/api/files",
    "title": "File upload",
    "name": "Upload_file",
    "group": "Utility",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>JWT</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Header example",
          "content": "{\n  \"Authorization\": \"Bearer JWT\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "docs",
            "description": "<p>File / Files (Use form data)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  \"docs\": \"file\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\n  \"success\": 1,\n  \"data\": {\n    \"files\": {\n      \"filename\": \"file name\",\n      \"permalink\": \"link of the file\",\n      \"_id\": \"file ID\"\n    }\n  },\n  \"message\": \"All files uploaded\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Server error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        },
        {
          "title": "Wrong credentials",
          "content": "HTTP/1.1 401 Not authorized",
          "type": "json"
        },
        {
          "title": "Wrong form key",
          "content": "HTTP/1.1 422 Unprocessable entity",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "src/server/app/modules/files/controller.js",
    "groupTitle": "Utility"
  }
] });
