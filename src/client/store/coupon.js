export const getters = {
  getCoupons: state => {
    return state.coupons;
  }
};

export const mutations = {
  setCoupons: (state, coupons) => {
    state.coupons = coupons;
  },
  addCoupon: (state, coupon) => {
    state.coupons.push(coupon);
  },
  editCoupon: (state, coupon) => {
    state.coupons = state.coupons.filter(el => el._id !== coupon._id);
    state.coupons.push(coupon);
  },
  deleteCoupon: (state, coupon) => {
    state.coupons = state.coupons.filter(el => el._id !== coupon._id);
  }
};

export const actions = {};

export const state = () => ({
  coupons: []
});
