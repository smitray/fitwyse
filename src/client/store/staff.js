export const getters = {
  getStaffSpecs: state => {
    return state.staffspecs;
  },
  getStaffs: state => {
    return state.staffs;
  }
};

export const mutations = {
  setStaffSpecs: (state, staffspecs) => {
    state.staffspecs = staffspecs;
  },
  addStaffSpec: (state, staffspec) => {
    state.staffspecs.push(staffspec);
  },
  editStaffSpec: (state, staffspec) => {
    state.staffspecs = state.staffspecs.filter(el => el._id !== staffspec._id);
    state.staffspecs.push(staffspec);
  },
  deleteStaffSpec: (state, staffspec) => {
    state.staffspecs = state.staffspecs.filter(el => el._id !== staffspec._id);
  },
  setStaffs: (state, staffs) => {
    state.staffs = staffs;
  },
  addStaff: (state, staff) => {
    state.staffs.push(staff);
  },
  editStaff: (state, staff) => {
    state.staffs = state.staffs.filter(el => el._id !== staff._id);
    state.staffs.push(staff);
  },
  deleteStaff: (state, staff) => {
    state.staffs = state.staffs.filter(el => el._id !== staff._id);
  }
};

export const actions = {};

export const state = () => ({
  staffspecs: [],
  staffs: []
});
