export const getters = {
  getLeadSources: state => {
    return state.leadsources;
  },
  getLeads: state => {
    return state.leads;
  }
};

export const mutations = {
  setLeadSources: (state, leadsources) => {
    state.leadsources = leadsources;
  },
  addLeadSources: (state, leadsource) => {
    state.leadsources.push(leadsource);
  },
  editLeadSources: (state, leadsource) => {
    state.leadsources = state.leadsources.filter(
      el => el._id !== leadsource._id
    );
    state.leadsources.push(leadsource);
  },
  deleteLeadSources: (state, leadsource) => {
    state.leadsources = state.leadsources.filter(
      el => el._id !== leadsource._id
    );
  },
  setLeads: (state, leads) => {
    state.leads = leads;
  },
  addLead: (state, lead) => {
    state.leads.push(lead);
  },
  editLead: (state, lead) => {
    state.leads = state.leads.filter(el => el._id !== lead._id);
    state.leads.push(lead);
  },
  deleteLead: (state, lead) => {
    state.leads = state.leads.filter(el => el._id !== lead._id);
  }
};

export const actions = {};

export const state = () => ({
  leadsources: [],
  leads: []
});
