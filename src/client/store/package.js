export const getters = {
  getPackages: state => {
    return state.packages;
  }
};

export const mutations = {
  setPackages: (state, packages) => {
    state.packages = packages;
  },
  addPackage: (state, packageSin) => {
    state.packages.push(packageSin);
  },
  editPackage: (state, packageSin) => {
    state.packages = state.packages.filter(el => el._id !== packageSin._id);
    state.packages.push(packageSin);
  },
  deletePackage: (state, packageSin) => {
    state.packages = state.packages.filter(el => el._id !== packageSin._id);
  }
};

export const actions = {};

export const state = () => ({
  packages: []
});
