export const getters = {
  getFitnessGoals: state => {
    return state.fitnessGoals;
  },
  getMembershipTypes: state => {
    return state.membershipTypes;
  },
  getMembers: state => {
    return state.members;
  }
};

export const mutations = {
  setFitnessGoals: (state, fitnessGoals) => {
    state.fitnessGoals = fitnessGoals;
  },
  addFitnessGoal: (state, fitnessGoal) => {
    state.fitnessGoals.push(fitnessGoal);
  },
  editFitnessGoal: (state, fitnessGoal) => {
    state.fitnessGoals = state.fitnessGoals.filter(
      el => el._id !== fitnessGoal._id
    );
    state.fitnessGoals.push(fitnessGoal);
  },
  deleteFitnessGoal: (state, fitnessGoal) => {
    state.fitnessGoals = state.fitnessGoals.filter(
      el => el._id !== fitnessGoal._id
    );
  },
  setMembershipTypes: (state, membershipTypes) => {
    state.membershipTypes = membershipTypes;
  },
  addMembershipType: (state, membershipType) => {
    state.membershipTypes.push(membershipType);
  },
  editMembershipType: (state, membershipType) => {
    state.membershipTypes = state.membershipTypes.filter(
      el => el._id !== membershipType._id
    );
    state.membershipTypes.push(membershipType);
  },
  deleteMembershipType: (state, membershipType) => {
    state.membershipTypes = state.membershipTypes.filter(
      el => el._id !== membershipType._id
    );
  },
  setMembers: (state, members) => {
    state.members = members;
  },
  addMember: (state, member) => {
    state.members.push(member);
  },
  editMember: (state, member) => {
    state.members = state.members.filter(el => el._id !== member._id);
    state.members.push(member);
  },
  deleteMember: (state, member) => {
    state.members = state.members.filter(el => el._id !== member._id);
  }
};

export const actions = {};

export const state = () => ({
  members: [],
  membershipTypes: [],
  fitnessGoals: []
});
