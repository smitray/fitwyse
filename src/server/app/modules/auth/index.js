export * as authRouteProps from './router';
export {
  authSingle,
  authSingleOther,
  authUpdate,
  authLocal,
  authSocial,
  createMember,
  getMember,
  singleMember,
  createtrainer,
  gettrainer,
  singletrainer,
  authDelete,
  updateMember,
  deleteMember,
  updatetrainer,
  deletetrainer
} from './controller';
export { default as authModel } from './auth.model';
export { default as authCrud } from './auth.service';
