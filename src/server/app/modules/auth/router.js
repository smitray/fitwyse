import { isAuthenticated } from '@mid';

import {
  authSingle,
  authSingleOther,
  authUpdate,
  authLocal,
  authSocial,
  createMember,
  getMember,
  singleMember,
  createtrainer,
  gettrainer,
  singletrainer,
  authDelete,
  updateMember,
  deleteMember,
  updatetrainer,
  deletetrainer
} from './';

export const baseUrl = '/api';

export const routes = [
  {
    method: 'POST',
    route: '/member',
    handlers: [
      isAuthenticated,
      createMember
    ]
  },
  {
    method: 'GET',
    route: '/member',
    handlers: [
      isAuthenticated,
      getMember
    ]
  },
  {
    method: 'GET',
    route: '/member/:id',
    handlers: [
      isAuthenticated,
      singleMember
    ]
  },
  {
    method: 'PUT',
    route: '/member/:id',
    handlers: [
      isAuthenticated,
      updateMember
    ]
  },
  {
    method: 'DELETE',
    route: '/member/:id',
    handlers: [
      isAuthenticated,
      deleteMember
    ]
  },
  {
    method: 'POST',
    route: '/trainer',
    handlers: [
      isAuthenticated,
      createtrainer
    ]
  },
  {
    method: 'GET',
    route: '/trainer',
    handlers: [
      isAuthenticated,
      gettrainer
    ]
  },
  {
    method: 'GET',
    route: '/trainer/:id',
    handlers: [
      isAuthenticated,
      singletrainer
    ]
  },
  {
    method: 'PUT',
    route: '/trainer/:id',
    handlers: [
      isAuthenticated,
      updatetrainer
    ]
  },
  {
    method: 'DELETE',
    route: '/trainer/:id',
    handlers: [
      isAuthenticated,
      deletetrainer
    ]
  },
  {
    method: 'GET',
    route: '/auth/',
    handlers: [
      isAuthenticated,
      authSingle
    ]
  },
  {
    method: 'GET',
    route: '/auth/:id',
    handlers: [
      isAuthenticated,
      authSingleOther
    ]
  },
  {
    method: 'DELETE',
    route: '/auth/:id',
    handlers: [
      isAuthenticated,
      authDelete
    ]
  },
  {
    method: 'PUT',
    route: '/auth/',
    handlers: [
      isAuthenticated,
      authUpdate
    ]
  },
  {
    method: 'POST',
    route: '/auth/local',
    handlers: [
      authLocal
    ]
  },
  {
    method: 'POST',
    route: '/auth/social',
    handlers: [
      authSocial
    ]
  }
];
