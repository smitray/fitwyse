import _ from 'lodash';
import { compareSync } from 'bcryptjs';
import { generateJwt } from '@utl';
import { authCrud } from './';
import { userCrud, memeberCrud, trainerCrud } from '../user';
import { packageCrud } from '../package';
import { paymentCrud } from '../payment';
import { leadCrud } from '../lead';
import { storyboardCrud } from '../storyboard';

let auth;
let jwt;
let user;

const tokenGenerator = async (data) => {
  jwt = await generateJwt(data);

  await authCrud.put({
    params: {
      qr: {
        _id: data.auth
      }
    },
    body: {
      jwt
    }
  });
  return jwt;
};

/**
@api {post} /api/auth/local Local Signup and Login
@apiName Local Authentication
@apiGroup Authentication
@apiParam {String} name User's full name
@apiParam {String} username User's username
@apiParam {String} password User's password
@apiParam {String} email User's email
@apiParam {String} [accType="user"] User's account type
@apiParam {Boolean} [signp=false] To toggle between signup and login
@apiParamExample {json} Signup
{
  "name": "John Doe",
  "username": "johndoe",
  "password": "test1234",
  "email": "john@doe.com",
  "accType": "admin",
  "signup": true
}
@apiParamExample {json} Login
{
  "username": "johndoe", //Username or email as value but key should be "username"
  "password": "test1234"
}
@apiSuccessExample {json} Success
{
  "success": 1,
  "data": {
    "token": "Bearer JWT token"
  },
  "message": "Loggedin successfully"
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 409 Record conflict
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const authLocal = async (ctx) => {
  const {
    name,
    username,
    password,
    email,
    accType,
    signup
  } = ctx.request.body;
  auth = await authCrud.single({
    qr: {
      $or: [
        {
          username
        },
        {
          email: username
        }
      ]
    }
  });
  if (signup && !auth) {
    try {
      user = await userCrud.create({
        full_name: name
      });
      auth = await authCrud.create({
        username,
        password,
        email,
        acc_type: accType,
        user: user._id
      });
    } catch (e) {
      ctx.throw(422, {
        success: 0,
        message: e.message
      });
    }
  } else if (signup && auth) {
    ctx.throw(409, {
      success: 0,
      message: 'Email or username already registered!!'
    });
  } else if (!auth) {
    ctx.throw(401, { success: 0, message: 'No user found' });
  } else if (auth && !compareSync(password, auth.password)) {
    ctx.throw(401, { success: 0, message: 'Password given is wrong' });
  }
  let uid = auth.user;
  if (user) {
    uid = user._id;
  }
  const token = await tokenGenerator({
    auth: auth._id,
    uid,
    acc_type: auth.acc_type,
    name
  });
  ctx.body = {
    success: 1,
    data: {
      token
    },
    message: 'Loggedin successfully'
  };
};

/**
@api {post} /api/auth/social Social Signup and Login
@apiName Social Authentication
@apiGroup Authentication
@apiParam {String} name User's full name
@apiParam {String} username User's username
@apiParam {String} email User's email
@apiParam {String} scId Social network ID
@apiParam {String} scToken Social network token
@apiParam {String} scType Name of the social network
@apiParamExample {json} Input
{
  "name": "John Doe",
  "username": "johndoe",
  "email": "john@doe.com",
  "scId": "123456789",
  "scToken": "123456789",
  "scType": "facebook" // Or twitter / google
}
@apiSuccessExample {json} Success
{
  "success": 1,
  "data": {
    "token": "Bearer JWT token"
  },
  "message": "Loggedin successfully"
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 409 Record conflict
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const authSocial = async (ctx) => {
  const {
    name,
    username,
    email,
    scId,
    scToken,
    scType
  } = ctx.request.body;

  const qr = {};
  qr.username = username;
  qr[`social.${scType}.id`] = scId;

  auth = await authCrud.single({
    qr
  });

  if (!auth) {
    try {
      user = await userCrud.create({
        full_name: name
      });
      auth = await authCrud.create({
        username,
        email,
        user: user._id,
        social: {
          [scType]: {
            id: scId,
            token: scToken
          }
        }
      });
    } catch (e) {
      ctx.throw(422, {
        success: 0,
        message: e.message
      });
    }
  }

  const token = await tokenGenerator({
    auth: auth._id,
    uid: user._id,
    acc_type: auth.acc_type
  });
  ctx.body = {
    success: 1,
    data: {
      token
    },
    message: 'Loggedin successfully'
  };
};

/**
@api {get} /api/auth Get my details
@apiName Get me
@apiGroup User
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  "Authorization": "Bearer JWT"
}
@apiSuccessExample {json} Success
{
  "success": 1,
  "data": {
    "user": [Object]
  },
  "message": "User details fetched successfully"
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
 */

const authSingle = async (ctx) => {
  try {
    auth = await authCrud.single({
      qr: {
        _id: ctx.state.user.auth
      },
      select: 'username email acc_type, user',
      populate: [
        {
          path: 'user',
          model: 'userModel',
          select: 'full_name dp',
          populate: [
            {
              path: 'dp',
              model: 'filesModel'
            }
          ]
        }
      ]
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        user: auth
      },
      message: 'Fetched my details successfully'
    };
  }
};

/**
@api {get} /api/auth/:id Get other's details
@apiName Get other
@apiGroup User
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  "Authorization": "Bearer JWT"
}
@apiSuccessExample {json} Success
{
  "success": 1,
  "data": {
    "user": [Object]
  },
  "message": "User details fetched successfully"
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
 */

const authSingleOther = async (ctx) => {
  try {
    auth = await authCrud.single({
      qr: {
        _id: ctx.params.id
      },
      select: 'username email acc_type, user',
      populate: [
        {
          path: 'user',
          model: 'userModel',
          select: 'full_name dp',
          populate: [
            {
              path: 'dp',
              model: 'filesModel'
            }
          ]
        }
      ]
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        user: auth
      },
      message: 'User details fetched successfully'
    };
  }
};

/**
@api {put} /api/auth/ Update auth module
@apiName Get other
@apiGroup Authentication
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  "Authorization": "Bearer JWT"
}
@apiParam {String} password User's password
@apiParam {String} email User's email
@apiParamExample {json} Input
{
  "email": "john@doe.com",
  "password": "test123456"
}
@apiSuccessExample {json} Success
{
  "success": 1,
  "data": {
    "auth": [Object]
  },
  "message": "auth updated successfully"
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 422 Unprocessable entity
 */

const authUpdate = async (ctx) => {
  try {
    const body = _.pick(ctx.request.body, ['password', 'email']);
    auth = await authCrud.put({
      params: {
        qr: {
          _id: ctx.state.user.auth
        }
      },
      body
    });
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        auth
      },
      message: 'auth updated successfully'
    };
  }
};

/**
@api {post} /api/member Create member
@apiName Create member
@apiGroup Member
@apiSuccessExample {json} Success
{
  "success": 1,
  "data": {
    "token": "Bearer JWT token"
  },
  "message": "members created successfully"
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 409 Record conflict
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const createMember = async (ctx) => {
  let newMember;
  const {
    payMode,
    payDate,
    payReference,
    membershipType,
    pckg,
    coupon,
    additionalDiscount,
    activation,
    source,
    interest,
    fitnessGoal,
    fitnessOpt,
    height,
    weight,
    diet,
    healthProblem,
    gender,
    dob,
    name,
    email,
    phone,
    alternatePhone,
    presentAdd,
    permanentAdd,
    language,
    pregnancy,
    leadId,
    storyId,
    dp,
    trainer
  } = ctx.request.body;
  let strId;
  try {
    const payment = await paymentCrud.create({
      payMode,
      payDate,
      payReference,
      coupon,
      additionalDiscount
    });
    if (!storyId) {
      const story = await storyboardCrud.create({
        content: `Member created by ${ctx.state.user.name}`
      });
      strId = story._id;
    } else {
      strId = storyId;
    }
    const member = await memeberCrud.create({
      source,
      interest,
      fitnessGoal,
      fitnessOpt,
      alternatePhone,
      presentAdd,
      permanentAdd,
      language,
      pregnancy,
      height,
      weight,
      diet,
      healthProblem,
      package: pckg,
      payment: payment._id,
      story: strId,
      membershipType,
      activation,
      trainer
    });
    user = await userCrud.create({
      full_name: name,
      dob,
      gender,
      member: member._id,
      dp
    });
    newMember = await authCrud.create({
      email,
      phone,
      user: user._id,
      acc_type: 'member',
      username: name
    });
    if (leadId) {
      await leadCrud.delete({
        params: {
          qr: {
            _id: leadId
          }
        }
      });
    }
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        member: newMember
      },
      message: 'member created successfully'
    };
  }
};

/**
@api {get} /api/member Get all members
@apiName All members
@apiGroup Member
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  "Authorization": "Bearer JWT"
}
@apiSuccessExample {json} Success
{
  "success": 1,
  "data": {
    "members": [Object]
  },
  "message": "members fetched successfully"
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
 */

const getMember = async (ctx) => {
  try {
    auth = await authCrud.get({
      qr: {
        acc_type: 'member'
      },
      select: 'email phone user',
      populate: [
        {
          path: 'user',
          model: 'userModel',
          populate: [
            {
              path: 'member',
              model: 'memberModel',
              populate: [
                {
                  path: 'package',
                  model: 'packageModel',
                  populate: [
                    {
                      path: 'asignee',
                      model: 'userModel'
                    }
                  ]
                },
                {
                  path: 'payment',
                  model: 'paymentModel'
                },
                {
                  path: 'story',
                  model: 'storyboardModel'
                }
              ]
            }
          ]
        }
      ]
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        members: auth
      },
      message: 'members fetched successfully'
    };
  }
};

/**
@api {get} /api/member/:id Get single member
@apiName Get single member
@apiGroup Member
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  "Authorization": "Bearer JWT"
}
@apiSuccessExample {json} Success
{
  "success": 1,
  "data": {
    "member": [Object]
  },
  "message": "member fetched successfully"
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
 */

const singleMember = async (ctx) => {
  try {
    auth = await authCrud.get({
      qr: {
        _id: ctx.params.id
      },
      select: 'email phone user',
      populate: [
        {
          path: 'user',
          model: 'userModel',
          populate: [
            {
              path: 'member',
              model: 'memberModel',
              populate: [
                {
                  path: 'package',
                  model: 'packageModel',
                  populate: [
                    {
                      path: 'asignee',
                      model: 'userModel'
                    }
                  ]
                },
                {
                  path: 'payment',
                  model: 'paymentModel'
                },
                {
                  path: 'story',
                  model: 'storyboardModel'
                }
              ]
            }
          ]
        }
      ]
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        member: auth
      },
      message: 'members fetched successfully'
    };
  }
};

export const updateMember = async (ctx) => {

};

export const deleteMember = async (ctx) => {

};

export const updatetrainer = async (ctx) => {

};

export const deletetrainer = async (ctx) => {

};


export const createtrainer = async (ctx) => {
  const {
    email,
    phone,
    password,
    name,
    dp,
    gender,
    dob,
    role,
    specialization
  } = ctx.request.body;

  try {
    const trainer = await trainerCrud.create({
      role,
      specialization
    });
    user = await userCrud.create({
      full_name: name,
      dp,
      gender,
      dob,
      trainer: trainer._id
    });
    auth = await authCrud.create({
      email,
      password,
      phone,
      acc_type: 'trainer',
      user: user._id
    });

    ctx.body = {
      success: 1,
      data: {
        trainer: auth
      },
      message: 'trainer created successfully'
    };
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  }
};

export const gettrainer = async (ctx) => {
  try {
    auth = await authCrud.get({
      qr: {
        acc_type: 'trainer'
      },
      select: 'username email phone user',
      populate: [{
        path: 'user',
        model: 'userModel',
        select: 'full_name dp trainer gender dob',
        populate: [{
          path: 'trainer',
          model: 'trainerModel'
        }, {
          path: 'dp',
          model: 'filesModel'
        }]
      }]
    });

    ctx.body = {
      success: 1,
      data: {
        trainers: auth
      },
      message: 'trainers fetched successfully'
    };
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  }
};

export const singletrainer = async (ctx) => {
  try {
    auth = await authCrud.get({
      qr: {
        _id: ctx.params.id
      },
      select: 'username email phone user',
      populate: [{
        path: 'user',
        model: 'userModel',
        select: 'full_name dp trainer gender dob',
        populate: [{
          path: 'trainer',
          model: 'trainerModel'
        }, {
          path: 'dp',
          model: 'filesModel'
        }]
      }]
    });

    ctx.body = {
      success: 1,
      data: {
        trainer: auth
      },
      message: 'trainer fetched successfully'
    };
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  }
};

export const authDelete = async (ctx) => {
  try {
    auth = await authCrud.delete({
      params: {
        qr: {
          _id: ctx.params.id
        }
      }
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        auth
      },
      message: 'auth deleted successfully'
    };
  }
};

export {
  authSingle,
  authSingleOther,
  authUpdate,
  authLocal,
  authSocial,
  createMember,
  getMember,
  singleMember
};
