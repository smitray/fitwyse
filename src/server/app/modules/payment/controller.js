import { paymentCrud } from './';


let payments;
let payment;
let paymentNew;

const paymentAll = async (ctx) => {
  try {
    payments = await paymentCrud.get();
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        payments
      },
      message: 'payments fetched successfully'
    };
  }
};

const paymentSingle = async (ctx) => {
  try {
    payment = await paymentCrud.single({
      qr: {
        _id: ctx.params.id
      }
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        payment
      },
      message: 'payment fetched successfully'
    };
  }
};

const paymentCreate = async (ctx) => {
  try {
    paymentNew = await paymentCrud.create(ctx.request.body);
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        payment: paymentNew
      },
      message: 'payment created successfully'
    };
  }
};

const paymentUpdate = async (ctx) => {
  try {
    payment = await paymentCrud.put({
      params: {
        qr: {
          _id: ctx.params.id
        }
      },
      body: ctx.request.body
    });
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        payment
      },
      message: 'payment updated successfully'
    };
  }
};

const paymentDelete = async (ctx) => {
  try {
    payment = await paymentCrud.delete({
      params: {
        qr: {
          _id: ctx.params.id
        }
      }
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        payment
      },
      message: 'payment deleted successfully'
    };
  }
};

export {
  paymentAll,
  paymentSingle,
  paymentCreate,
  paymentUpdate,
  paymentDelete
};
