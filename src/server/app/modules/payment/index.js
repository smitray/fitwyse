export * as paymentRouteProps from './router';
export {
  paymentAll,
  paymentSingle,
  paymentCreate,
  paymentUpdate,
  paymentDelete
} from './controller';
export { default as paymentModel } from './payment.model';
export { default as paymentCrud } from './payment.service';

