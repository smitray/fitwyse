import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

const paymentSchema = new mongoose.Schema({
  payMode: String,
  payDate: Date,
  payReference: String,
  additionalDiscount: String,
  coupon: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'couponModel',
    default: null
  }
});

paymentSchema.plugin(uniqueValidator);
paymentSchema.plugin(timestamp);

const paymentModel = mongoose.model('paymentModel', paymentSchema);

export default paymentModel;
