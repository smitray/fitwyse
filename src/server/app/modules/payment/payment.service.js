import { Crud } from '@utl';

import paymentModel from './payment.model';

class Servicepayment extends Crud {
  constructor(model) {
    super(model);
    this.model = model;
  }
}

const paymentCrud = new Servicepayment(paymentModel);
export default paymentCrud;
