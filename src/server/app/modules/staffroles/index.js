export * as staffrolesRouteProps from './router';
export {
  staffrolesAll,
  staffrolesSingle,
  staffrolesCreate,
  staffrolesUpdate,
  staffrolesDelete
} from './controller';
export { default as staffrolesModel } from './staffroles.model';
export { default as staffrolesCrud } from './staffroles.service';

