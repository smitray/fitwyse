import { Crud } from '@utl';

import staffrolesModel from './staffroles.model';

class Servicestaffroles extends Crud {
  constructor(model) {
    super(model);
    this.model = model;
  }
}

const staffrolesCrud = new Servicestaffroles(staffrolesModel);
export default staffrolesCrud;
