// import { isAuthenticated } from '@mid';

import {
  staffrolesAll,
  staffrolesSingle,
  staffrolesCreate,
  staffrolesUpdate,
  staffrolesDelete
} from './';

export const baseUrl = '/api/staffroles';

export const routes = [
  {
    method: 'GET',
    route: '/',
    handlers: [
      staffrolesAll
    ]
  },
  {
    method: 'GET',
    route: '/:id',
    handlers: [
      staffrolesSingle
    ]
  },
  {
    method: 'PUT',
    route: '/:id',
    handlers: [
      staffrolesUpdate
    ]
  },
  {
    method: 'DELETE',
    route: '/:id',
    handlers: [
      staffrolesDelete
    ]
  },
  {
    method: 'POST',
    route: '/',
    handlers: [
      staffrolesCreate
    ]
  }
];
