import { staffrolesCrud } from './';


let staffroless;
let staffroles;
let staffrolesNew;

/**
@api {get} /api/staffroles Get all staffroles
@apiName Get all staffroles
@apiGroup Staffroles
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    staffroless: [Object]
  },
  message: staffroles details fetched successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
 */

const staffrolesAll = async (ctx) => {
  try {
    staffroless = await staffrolesCrud.get();
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        staffroless
      },
      message: 'staffroless fetched successfully'
    };
  }
};


/**
@api {get} /api/staffroles/:id Get single staffroles
@apiName Get single staffroles
@apiGroup Staffroles
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    staffroles: [Object]
  },
  message: staffroles details fetched successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
 */

const staffrolesSingle = async (ctx) => {
  try {
    staffroles = await staffrolesCrud.single({
      qr: {
        _id: ctx.params.id
      }
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        staffroles
      },
      message: 'staffroles fetched successfully'
    };
  }
};

/**
@api {post} /api/staffroles Create staffroles
@apiName Create staffroles
@apiGroup Staffroles
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiParam {String} roles Staff roles
@apiParamExample {json} Input
{
  "roles": "Admin"
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    staffroles: [Object]
  },
  message: staffroles created successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const staffrolesCreate = async (ctx) => {
  try {
    staffrolesNew = await staffrolesCrud.create(ctx.request.body);
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        staffroles: staffrolesNew
      },
      message: 'staffroles created successfully'
    };
  }
};

/**
@api {put} /api/staffroles/:id Update staffroles
@apiName Update staffroles
@apiGroup Staffroles
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiParam {String} ExampleFieldName Example details
@apiParamExample {json} Input
{
  ExampleFieldName: Example Value
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    staffroles: [Object]
  },
  message: staffroles updated successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const staffrolesUpdate = async (ctx) => {
  try {
    staffroles = await staffrolesCrud.put({
      params: {
        qr: {
          _id: ctx.params.id
        }
      },
      body: ctx.request.body
    });
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        staffroles
      },
      message: 'staffroles updated successfully'
    };
  }
};

/**
@api {delete} /api/staffroles/:id Delete staffroles
@apiName Delete staffroles
@apiGroup Staffroles
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    staffroles: [Object]
  },
  message: staffroles deleted successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const staffrolesDelete = async (ctx) => {
  try {
    staffroles = await staffrolesCrud.delete({
      params: {
        qr: {
          _id: ctx.params.id
        }
      }
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        staffroles
      },
      message: 'staffroles deleted successfully'
    };
  }
};

export {
  staffrolesAll,
  staffrolesSingle,
  staffrolesCreate,
  staffrolesUpdate,
  staffrolesDelete
};
