import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

const staffrolesSchema = new mongoose.Schema({
  roles: String
});

staffrolesSchema.plugin(uniqueValidator);
staffrolesSchema.plugin(timestamp);

const staffrolesModel = mongoose.model('staffrolesModel', staffrolesSchema);

export default staffrolesModel;
