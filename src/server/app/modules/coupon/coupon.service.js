import { Crud } from '@utl';

import couponModel from './coupon.model';

class Servicecoupon extends Crud {
  constructor(model) {
    super(model);
    this.model = model;
  }
}

const couponCrud = new Servicecoupon(couponModel);
export default couponCrud;
