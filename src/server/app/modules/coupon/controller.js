import { couponCrud } from './';

let coupons;
let coupon;
let couponNew;

/**
@api {get} /api/coupon/:member Get all coupon
@apiName Get all coupon
@apiGroup Coupon
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    coupons: [Object]
  },
  message: coupon details fetched successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
 */

const couponAll = async ctx => {
  try {
    coupons = await couponCrud.get({
      qr: {
        membershipType: ctx.params.id
      }
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        coupons
      },
      message: 'coupons fetched successfully'
    };
  }
};

/**
@api {post} /api/coupon Create coupon
@apiName Create coupon
@apiGroup Coupon
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiParam {String} code Coupon code
@apiParam {String} membershipType Membership type
@apiParam {String} couponValue Coupon value
@apiParam {Date} validFrom Coupon code
@apiParam {Date} validTo Coupon code
@apiParam {String} desc Coupon description
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    coupon: [Object]
  },
  message: coupon created successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const couponCreate = async ctx => {
  try {
    couponNew = await couponCrud.create(ctx.request.body);
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        coupon: couponNew
      },
      message: 'coupon created successfully'
    };
  }
};

/**
@api {put} /api/coupon/:id Update coupon
@apiName Update coupon
@apiGroup Coupon
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiParam {String} ExampleFieldName Example details
@apiParamExample {json} Input
{
  ExampleFieldName: Example Value
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    coupon: [Object]
  },
  message: coupon updated successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const couponUpdate = async ctx => {
  try {
    coupon = await couponCrud.put({
      params: {
        qr: {
          _id: ctx.params.id
        }
      },
      body: ctx.request.body
    });
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        coupon
      },
      message: 'coupon updated successfully'
    };
  }
};

/**
@api {delete} /api/coupon/:id Delete coupon
@apiName Delete coupon
@apiGroup Coupon
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    coupon: [Object]
  },
  message: coupon deleted successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const couponDelete = async ctx => {
  try {
    coupon = await couponCrud.delete({
      params: {
        qr: {
          _id: ctx.params.id
        }
      }
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        coupon
      },
      message: 'coupon deleted successfully'
    };
  }
};

const couponGet = async ctx => {
  try {
    coupons = await couponCrud.get();
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        coupons
      },
      message: 'coupons fetched successfully'
    };
  }
};

export { couponGet, couponAll, couponCreate, couponUpdate, couponDelete };
