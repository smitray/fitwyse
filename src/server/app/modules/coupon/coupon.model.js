import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

const couponSchema = new mongoose.Schema({
  code: String,
  membershipType: String,
  couponValue: String,
  validFrom: Date,
  validTo: Date,
  desc: String
});

couponSchema.plugin(uniqueValidator);
couponSchema.plugin(timestamp);

const couponModel = mongoose.model('couponModel', couponSchema);

export default couponModel;
