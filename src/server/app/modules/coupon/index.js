export * as couponRouteProps from './router';
export {
  couponGet,
  couponAll,
  couponCreate,
  couponUpdate,
  couponDelete
} from './controller';
export { default as couponModel } from './coupon.model';
export { default as couponCrud } from './coupon.service';
