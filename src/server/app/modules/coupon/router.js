import { isAuthenticated } from '@mid';

import {
  couponGet,
  couponAll,
  couponCreate,
  couponUpdate,
  couponDelete
} from './';

export const baseUrl = '/api/coupon';

export const routes = [
  {
    method: 'GET',
    route: '/',
    handlers: [isAuthenticated, couponGet]
  },
  {
    method: 'GET',
    route: '/:id',
    handlers: [isAuthenticated, couponAll]
  },
  {
    method: 'PUT',
    route: '/:id',
    handlers: [isAuthenticated, couponUpdate]
  },
  {
    method: 'DELETE',
    route: '/:id',
    handlers: [isAuthenticated, couponDelete]
  },
  {
    method: 'POST',
    route: '/',
    handlers: [isAuthenticated, couponCreate]
  }
];
