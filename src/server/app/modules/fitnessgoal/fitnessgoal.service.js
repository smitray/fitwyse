import { Crud } from '@utl';

import fitnessgoalModel from './fitnessgoal.model';

class Servicefitnessgoal extends Crud {
  constructor(model) {
    super(model);
    this.model = model;
  }
}

const fitnessgoalCrud = new Servicefitnessgoal(fitnessgoalModel);
export default fitnessgoalCrud;
