import { isAuthenticated } from '@mid';

import {
  fitnessgoalAll,
  fitnessgoalSingle,
  fitnessgoalCreate,
  fitnessgoalUpdate,
  fitnessgoalDelete
} from './';

export const baseUrl = '/api/fitnessgoal';

export const routes = [
  {
    method: 'GET',
    route: '/',
    handlers: [
      isAuthenticated,
      fitnessgoalAll
    ]
  },
  {
    method: 'GET',
    route: '/:id',
    handlers: [
      isAuthenticated,
      fitnessgoalSingle
    ]
  },
  {
    method: 'PUT',
    route: '/:id',
    handlers: [
      isAuthenticated,
      fitnessgoalUpdate
    ]
  },
  {
    method: 'DELETE',
    route: '/:id',
    handlers: [
      isAuthenticated,
      fitnessgoalDelete
    ]
  },
  {
    method: 'POST',
    route: '/',
    handlers: [
      isAuthenticated,
      fitnessgoalCreate
    ]
  }
];
