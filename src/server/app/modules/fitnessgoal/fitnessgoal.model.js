import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

const fitnessgoalSchema = new mongoose.Schema({
  goals: {
    type: String
  }
});

fitnessgoalSchema.plugin(uniqueValidator);
fitnessgoalSchema.plugin(timestamp);

const fitnessgoalModel = mongoose.model('fitnessgoalModel', fitnessgoalSchema);

export default fitnessgoalModel;
