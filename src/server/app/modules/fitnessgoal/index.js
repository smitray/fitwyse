export * as fitnessgoalRouteProps from './router';
export {
  fitnessgoalAll,
  fitnessgoalSingle,
  fitnessgoalCreate,
  fitnessgoalUpdate,
  fitnessgoalDelete
} from './controller';
export { default as fitnessgoalModel } from './fitnessgoal.model';
export { default as fitnessgoalCrud } from './fitnessgoal.service';

