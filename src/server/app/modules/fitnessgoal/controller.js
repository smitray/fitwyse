import { fitnessgoalCrud } from './';


let fitnessgoals;
let fitnessgoal;
let fitnessgoalNew;

/**
@api {get} /api/fitnessgoal Get all fitnessgoal
@apiName Get all fitnessgoal
@apiGroup Fitnessgoal
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    fitnessgoals: [Object]
  },
  message: fitnessgoal details fetched successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
 */

const fitnessgoalAll = async (ctx) => {
  try {
    fitnessgoals = await fitnessgoalCrud.get();
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        fitnessgoals
      },
      message: 'fitnessgoals fetched successfully'
    };
  }
};


/**
@api {get} /api/fitnessgoal/:id Get single fitnessgoal
@apiName Get single fitnessgoal
@apiGroup Fitnessgoal
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    fitnessgoal: [Object]
  },
  message: fitnessgoal details fetched successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
 */

const fitnessgoalSingle = async (ctx) => {
  try {
    fitnessgoal = await fitnessgoalCrud.single({
      qr: {
        _id: ctx.params.id
      }
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        fitnessgoal
      },
      message: 'fitnessgoal fetched successfully'
    };
  }
};

/**
@api {post} /api/fitnessgoal Create fitnessgoal
@apiName Create fitnessgoal
@apiGroup Fitnessgoal
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiParam {String} goals Fitness goal titles
@apiParamExample {json} Input
{
  "goals": "Weight gain"
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    fitnessgoal: [Object]
  },
  message: fitnessgoal created successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const fitnessgoalCreate = async (ctx) => {
  try {
    fitnessgoalNew = await fitnessgoalCrud.create(ctx.request.body);
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        fitnessgoal: fitnessgoalNew
      },
      message: 'fitnessgoal created successfully'
    };
  }
};

/**
@api {put} /api/fitnessgoal/:id Update fitnessgoal
@apiName Update fitnessgoal
@apiGroup Fitnessgoal
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiParam {String} ExampleFieldName Example details
@apiParamExample {json} Input
{
  ExampleFieldName: Example Value
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    fitnessgoal: [Object]
  },
  message: fitnessgoal updated successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const fitnessgoalUpdate = async (ctx) => {
  try {
    fitnessgoal = await fitnessgoalCrud.put({
      params: {
        qr: {
          _id: ctx.params.id
        }
      },
      body: ctx.request.body
    });
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        fitnessgoal
      },
      message: 'fitnessgoal updated successfully'
    };
  }
};

/**
@api {delete} /api/fitnessgoal/:id Delete fitnessgoal
@apiName Delete fitnessgoal
@apiGroup Fitnessgoal
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    fitnessgoal: [Object]
  },
  message: fitnessgoal deleted successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const fitnessgoalDelete = async (ctx) => {
  try {
    fitnessgoal = await fitnessgoalCrud.delete({
      params: {
        qr: {
          _id: ctx.params.id
        }
      }
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        fitnessgoal
      },
      message: 'fitnessgoal deleted successfully'
    };
  }
};

export {
  fitnessgoalAll,
  fitnessgoalSingle,
  fitnessgoalCreate,
  fitnessgoalUpdate,
  fitnessgoalDelete
};
