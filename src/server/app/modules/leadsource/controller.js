import { leadsourceCrud } from './';


let leadsources;
let leadsource;
let leadsourceNew;

/**
@api {get} /api/leadsource Get all leadsource
@apiName Get all leadsource
@apiGroup Leadsource
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    leadsources: [Object]
  },
  message: leadsource details fetched successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
 */

const leadsourceAll = async (ctx) => {
  try {
    leadsources = await leadsourceCrud.get();
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        leadsources
      },
      message: 'leadsources fetched successfully'
    };
  }
};


/**
@api {get} /api/leadsource/:id Get single leadsource
@apiName Get single leadsource
@apiGroup Leadsource
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    leadsource: [Object]
  },
  message: leadsource details fetched successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
 */

const leadsourceSingle = async (ctx) => {
  try {
    leadsource = await leadsourceCrud.single({
      qr: {
        _id: ctx.params.id
      }
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        leadsource
      },
      message: 'leadsource fetched successfully'
    };
  }
};

/**
@api {post} /api/leadsource Create leadsource
@apiName Create leadsource
@apiGroup Leadsource
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiParam {String} ExampleFieldName Example details
@apiParamExample {json} Input
{
  ExampleFieldName: Example Value
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    leadsource: [Object]
  },
  message: leadsource created successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const leadsourceCreate = async (ctx) => {
  try {
    leadsourceNew = await leadsourceCrud.create(ctx.request.body);
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        leadsource: leadsourceNew
      },
      message: 'leadsource created successfully'
    };
  }
};

/**
@api {post} /api/leadsource/:id Update leadsource
@apiName Update leadsource
@apiGroup Leadsource
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiParam {String} ExampleFieldName Example details
@apiParamExample {json} Input
{
  ExampleFieldName: Example Value
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    leadsource: [Object]
  },
  message: leadsource updated successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const leadsourceUpdate = async (ctx) => {
  try {
    leadsource = await leadsourceCrud.put({
      params: {
        qr: {
          _id: ctx.params.id
        }
      },
      body: ctx.request.body
    });
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        leadsource
      },
      message: 'leadsource updated successfully'
    };
  }
};

/**
@api {post} /api/leadsource/:id Delete leadsource
@apiName Delete leadsource
@apiGroup Leadsource
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    leadsource: [Object]
  },
  message: leadsource deleted successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const leadsourceDelete = async (ctx) => {
  try {
    leadsource = await leadsourceCrud.delete({
      params: {
        qr: {
          _id: ctx.params.id
        }
      }
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        leadsource
      },
      message: 'leadsource deleted successfully'
    };
  }
};

export {
  leadsourceAll,
  leadsourceSingle,
  leadsourceCreate,
  leadsourceUpdate,
  leadsourceDelete
};
