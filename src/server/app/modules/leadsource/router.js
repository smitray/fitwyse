import { isAuthenticated } from '@mid';

import {
  leadsourceAll,
  leadsourceSingle,
  leadsourceCreate,
  leadsourceUpdate,
  leadsourceDelete
} from './';

export const baseUrl = '/api/leadsource';

export const routes = [
  {
    method: 'GET',
    route: '/',
    handlers: [
      isAuthenticated,
      leadsourceAll
    ]
  },
  {
    method: 'GET',
    route: '/:id',
    handlers: [
      isAuthenticated,
      leadsourceSingle
    ]
  },
  {
    method: 'PUT',
    route: '/:id',
    handlers: [
      isAuthenticated,
      leadsourceUpdate
    ]
  },
  {
    method: 'DELETE',
    route: '/:id',
    handlers: [
      isAuthenticated,
      leadsourceDelete
    ]
  },
  {
    method: 'POST',
    route: '/',
    handlers: [
      isAuthenticated,
      leadsourceCreate
    ]
  }
];
