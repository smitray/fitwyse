import { Crud } from '@utl';

import leadsourceModel from './leadsource.model';

class Serviceleadsource extends Crud {
  constructor(model) {
    super(model);
    this.model = model;
  }
}

const leadsourceCrud = new Serviceleadsource(leadsourceModel);
export default leadsourceCrud;
