import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

const leadsourceSchema = new mongoose.Schema({
  source: {
    type: String
  }
});

leadsourceSchema.plugin(uniqueValidator);
leadsourceSchema.plugin(timestamp);

const leadsourceModel = mongoose.model('leadsourceModel', leadsourceSchema);

export default leadsourceModel;
