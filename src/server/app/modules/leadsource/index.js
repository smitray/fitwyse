export * as leadsourceRouteProps from './router';
export {
  leadsourceAll,
  leadsourceSingle,
  leadsourceCreate,
  leadsourceUpdate,
  leadsourceDelete
} from './controller';
export { default as leadsourceModel } from './leadsource.model';
export { default as leadsourceCrud } from './leadsource.service';

