import { Crud } from '@utl';

import leadModel from './lead.model';

class Servicelead extends Crud {
  constructor(model) {
    super(model);
    this.model = model;
  }
}

const leadCrud = new Servicelead(leadModel);
export default leadCrud;
