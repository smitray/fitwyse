import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

const leadSchema = new mongoose.Schema({
  phone: {
    type: String,
    default: null
  },
  dp: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'filesModel',
    default: null
  },
  email: {
    type: String,
    default: null
  },
  name: String,
  gender: String,
  source: String,
  asignee: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'userModel',
    default: null
  },
  dob: Date,
  followUp: Date,
  interest: String,
  fitnessGoal: String,
  status: {
    type: String,
    default: 'New'
  },
  stage: String,
  subStage: String,
  trial: Date,
  style: String,
  story: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'storyboardModel',
    default: null
  },
  note: String
});

leadSchema.plugin(uniqueValidator);
leadSchema.plugin(timestamp);

const leadModel = mongoose.model('leadModel', leadSchema);

export default leadModel;
