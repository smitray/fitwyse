import { leadCrud } from './';
import { storyboardCrud } from '../storyboard';

let leads;
let lead;
let leadNew;

/**
@api {get} /api/lead Get all leads
@apiName All Leads
@apiGroup Lead
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  "Authorization": "Bearer JWT"
}
@apiSuccessExample {json} Success
{
  "success": 1,
  "data": {
    "leads": [Object]
  },
  "message": "leads fetched successfully"
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
 */

const leadAll = async (ctx) => {
  try {
    leads = await leadCrud.get({
      populate: [{
        path: 'dp',
        model: 'filesModel'
      }, {
        path: 'asignee',
        model: 'userModel'
      }, {
        path: 'story',
        model: 'storyboardModel'
      }]
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        leads
      },
      message: 'leads fetched successfully'
    };
  }
};

/**
@api {get} /api/lead/:id Get single lead
@apiName Get single lead
@apiGroup Lead
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  "Authorization": "Bearer JWT"
}
@apiSuccessExample {json} Success
{
  "success": 1,
  "data": {
    "lead": [Object]
  },
  "message": "lead fetched successfully"
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
 */

const leadSingle = async (ctx) => {
  try {
    lead = await leadCrud.single({
      qr: {
        _id: ctx.params.id
      },
      populate: [{
        path: 'dp',
        model: 'filesModel'
      }, {
        path: 'asignee',
        model: 'userModel'
      }, {
        path: 'story',
        model: 'storyboardModel'
      }]
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        lead
      },
      message: 'lead fetched successfully'
    };
  }
};

/**
@api {post} /api/lead Create lead
@apiName Create lead
@apiGroup Lead
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiParam {ObjectId} dp File ID
@apiParam {String} phone phone number of the lead
@apiParam {String} email email address of the lead
@apiParam {String} name full name of the lead
@apiParam {String} gender gender of the lead
@apiParam {String} source source type of the lead
@apiParamExample {json} Input
{
  "phone": "9804313438",
  "dp": [ObjectId],
  "email": "xyz@xyz.com",
  "name": "Smit Ray",
  "gender": "male",
  "source": "Walk in"
}
@apiSuccessExample {json} Success
{
  "success": 1,
  "data": {
    "token": "Bearer JWT token"
  },
  "message": "lead created successfully"
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 409 Record conflict
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const leadCreate = async (ctx) => {
  try {
    const story = await storyboardCrud.create({
      content: `Lead created by ${ctx.state.user.name}`
    });
    leadNew = await leadCrud.create({
      ...ctx.request.body,
      asignee: ctx.state.user.auth,
      story: story._id
    });
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        lead: leadNew
      },
      message: 'lead created successfully'
    };
  }
};

/**
@api {put} /api/lead Update lead
@apiName Update lead
@apiGroup Lead
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  "Authorization": "Bearer JWT"
}
@apiSuccessExample {json} Success
{
  "success": 1,
  "data": {
    "auth": [Object]
  },
  "message": "lead updated successfully"
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 422 Unprocessable entity
 */

const leadUpdate = async (ctx) => {
  try {
    lead = await leadCrud.put({
      params: {
        qr: {
          _id: ctx.params.id
        }
      },
      body: ctx.request.body
    });
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        lead
      },
      message: 'lead updated successfully'
    };
  }
};

const leadDelete = async (ctx) => {
  try {
    lead = await leadCrud.delete({
      params: {
        qr: {
          _id: ctx.params.id
        }
      }
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        lead
      },
      message: 'lead deleted successfully'
    };
  }
};

export {
  leadAll,
  leadSingle,
  leadCreate,
  leadUpdate,
  leadDelete
};
