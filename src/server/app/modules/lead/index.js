export * as leadRouteProps from './router';
export {
  leadAll,
  leadSingle,
  leadCreate,
  leadUpdate,
  leadDelete
} from './controller';
export { default as leadModel } from './lead.model';
export { default as leadCrud } from './lead.service';

