import { Crud } from '@utl';

import membershiptypeModel from './membershiptype.model';

class Servicemembershiptype extends Crud {
  constructor(model) {
    super(model);
    this.model = model;
  }
}

const membershiptypeCrud = new Servicemembershiptype(membershiptypeModel);
export default membershiptypeCrud;
