import { membershiptypeCrud } from './';


let membershiptypes;
let membershiptype;
let membershiptypeNew;

/**
@api {get} /api/membershiptype Get all membershiptype
@apiName Get all membershiptype
@apiGroup Membershiptype
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    membershiptypes: [Object]
  },
  message: membershiptype details fetched successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
 */

const membershiptypeAll = async (ctx) => {
  try {
    membershiptypes = await membershiptypeCrud.get();
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        membershiptypes
      },
      message: 'membershiptypes fetched successfully'
    };
  }
};


/**
@api {get} /api/membershiptype/:id Get single membershiptype
@apiName Get single membershiptype
@apiGroup Membershiptype
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    membershiptype: [Object]
  },
  message: membershiptype details fetched successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
 */

const membershiptypeSingle = async (ctx) => {
  try {
    membershiptype = await membershiptypeCrud.single({
      qr: {
        _id: ctx.params.id
      }
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        membershiptype
      },
      message: 'membershiptype fetched successfully'
    };
  }
};

/**
@api {post} /api/membershiptype Create membershiptype
@apiName Create membershiptype
@apiGroup Membershiptype
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiParam {String} memberType Membership type title
@apiParamExample {json} Input
{
  "memberType": "Membership"
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    membershiptype: [Object]
  },
  message: membershiptype created successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const membershiptypeCreate = async (ctx) => {
  try {
    membershiptypeNew = await membershiptypeCrud.create(ctx.request.body);
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        membershiptype: membershiptypeNew
      },
      message: 'membershiptype created successfully'
    };
  }
};

/**
@api {put} /api/membershiptype/:id Update membershiptype
@apiName Update membershiptype
@apiGroup Membershiptype
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiParam {String} ExampleFieldName Example details
@apiParamExample {json} Input
{
  ExampleFieldName: Example Value
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    membershiptype: [Object]
  },
  message: membershiptype updated successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const membershiptypeUpdate = async (ctx) => {
  try {
    membershiptype = await membershiptypeCrud.put({
      params: {
        qr: {
          _id: ctx.params.id
        }
      },
      body: ctx.request.body
    });
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        membershiptype
      },
      message: 'membershiptype updated successfully'
    };
  }
};

/**
@api {delete} /api/membershiptype/:id Delete membershiptype
@apiName Delete membershiptype
@apiGroup Membershiptype
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    membershiptype: [Object]
  },
  message: membershiptype deleted successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const membershiptypeDelete = async (ctx) => {
  try {
    membershiptype = await membershiptypeCrud.delete({
      params: {
        qr: {
          _id: ctx.params.id
        }
      }
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        membershiptype
      },
      message: 'membershiptype deleted successfully'
    };
  }
};

export {
  membershiptypeAll,
  membershiptypeSingle,
  membershiptypeCreate,
  membershiptypeUpdate,
  membershiptypeDelete
};
