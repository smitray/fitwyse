import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

const membershiptypeSchema = new mongoose.Schema({
  memberType: {
    type: String
  },
  slug: {
    type: String
  }
});

membershiptypeSchema.plugin(uniqueValidator);
membershiptypeSchema.plugin(timestamp);

const membershiptypeModel = mongoose.model('membershiptypeModel', membershiptypeSchema);

export default membershiptypeModel;
