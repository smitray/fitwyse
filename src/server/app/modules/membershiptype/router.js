import { isAuthenticated } from '@mid';

import {
  membershiptypeAll,
  membershiptypeSingle,
  membershiptypeCreate,
  membershiptypeUpdate,
  membershiptypeDelete
} from './';

export const baseUrl = '/api/membershiptype';

export const routes = [
  {
    method: 'GET',
    route: '/',
    handlers: [
      isAuthenticated,
      membershiptypeAll
    ]
  },
  {
    method: 'GET',
    route: '/:id',
    handlers: [
      isAuthenticated,
      membershiptypeSingle
    ]
  },
  {
    method: 'PUT',
    route: '/:id',
    handlers: [
      isAuthenticated,
      membershiptypeUpdate
    ]
  },
  {
    method: 'DELETE',
    route: '/:id',
    handlers: [
      isAuthenticated,
      membershiptypeDelete
    ]
  },
  {
    method: 'POST',
    route: '/',
    handlers: [
      isAuthenticated,
      membershiptypeCreate
    ]
  }
];
