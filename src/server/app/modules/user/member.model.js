import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

import { Crud } from '@utl';

const memberSchema = new mongoose.Schema({
  source: String,
  interest: String,
  fitnessGoal: String,
  height: String,
  weight: String,
  diet: String,
  healthProblem: String,
  activation: Date,
  fitnessOpt: String,
  alternatePhone: String,
  presentAdd: String,
  permanentAdd: String,
  language: String,
  pregnancy: {
    type: Boolean,
    default: false
  },
  package: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'packageModel',
    default: null
  },
  payment: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'paymentModel',
    default: null
  },
  story: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'storyboardModel',
    default: null
  },
  pkg: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'packageModel',
    default: null
  },
  membershipType: String,
  trainer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'authModel',
    default: null
  }
});

memberSchema.plugin(uniqueValidator);
memberSchema.plugin(timestamp);

const memberModel = mongoose.model('memberModel', memberSchema);

const memeberCrud = new Crud(memberModel);

export {
  memberModel,
  memeberCrud
};
