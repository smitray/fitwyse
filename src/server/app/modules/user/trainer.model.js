import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

import { Crud } from '@utl';

const trainerSchema = new mongoose.Schema({
  role: String,
  specialization: String
});

trainerSchema.plugin(uniqueValidator);
trainerSchema.plugin(timestamp);

const trainerModel = mongoose.model('trainerModel', trainerSchema);

const trainerCrud = new Crud(trainerModel);

export {
  trainerModel,
  trainerCrud
};
