import { Crud } from '@utl';

import specializationModel from './specialization.model';

class Servicespecialization extends Crud {
  constructor(model) {
    super(model);
    this.model = model;
  }
}

const specializationCrud = new Servicespecialization(specializationModel);
export default specializationCrud;
