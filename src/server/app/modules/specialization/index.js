export * as specializationRouteProps from './router';
export {
  specializationAll,
  specializationSingle,
  specializationCreate,
  specializationUpdate,
  specializationDelete
} from './controller';
export { default as specializationModel } from './specialization.model';
export { default as specializationCrud } from './specialization.service';

