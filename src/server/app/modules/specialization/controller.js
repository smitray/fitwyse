import { specializationCrud } from './';


let specializations;
let specialization;
let specializationNew;

/**
@api {get} /api/specialization Get all specialization
@apiName Get all specialization
@apiGroup Specialization
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    specializations: [Object]
  },
  message: specialization details fetched successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
 */

const specializationAll = async (ctx) => {
  try {
    specializations = await specializationCrud.get();
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        specializations
      },
      message: 'specializations fetched successfully'
    };
  }
};


/**
@api {get} /api/specialization/:id Get single specialization
@apiName Get single specialization
@apiGroup Specialization
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    specialization: [Object]
  },
  message: specialization details fetched successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
 */

const specializationSingle = async (ctx) => {
  try {
    specialization = await specializationCrud.single({
      qr: {
        _id: ctx.params.id
      }
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        specialization
      },
      message: 'specialization fetched successfully'
    };
  }
};

/**
@api {post} /api/specialization Create specialization
@apiName Create specialization
@apiGroup Specialization
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiParam {String} special Specialization title
@apiParamExample {json} Input
{
  "special": "Aerobics"
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    specialization: [Object]
  },
  message: specialization created successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const specializationCreate = async (ctx) => {
  try {
    specializationNew = await specializationCrud.create(ctx.request.body);
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        specialization: specializationNew
      },
      message: 'specialization created successfully'
    };
  }
};

/**
@api {put} /api/specialization/:id Update specialization
@apiName Update specialization
@apiGroup Specialization
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiParam {String} ExampleFieldName Example details
@apiParamExample {json} Input
{
  ExampleFieldName: Example Value
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    specialization: [Object]
  },
  message: specialization updated successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const specializationUpdate = async (ctx) => {
  try {
    specialization = await specializationCrud.put({
      params: {
        qr: {
          _id: ctx.params.id
        }
      },
      body: ctx.request.body
    });
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        specialization
      },
      message: 'specialization updated successfully'
    };
  }
};

/**
@api {delete} /api/specialization/:id Delete specialization
@apiName Delete specialization
@apiGroup Specialization
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    specialization: [Object]
  },
  message: specialization deleted successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const specializationDelete = async (ctx) => {
  try {
    specialization = await specializationCrud.delete({
      params: {
        qr: {
          _id: ctx.params.id
        }
      }
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        specialization
      },
      message: 'specialization deleted successfully'
    };
  }
};

export {
  specializationAll,
  specializationSingle,
  specializationCreate,
  specializationUpdate,
  specializationDelete
};
