// import { isAuthenticated } from '@mid';

import {
  specializationAll,
  specializationSingle,
  specializationCreate,
  specializationUpdate,
  specializationDelete
} from './';

export const baseUrl = '/api/specialization';

export const routes = [
  {
    method: 'GET',
    route: '/',
    handlers: [
      specializationAll
    ]
  },
  {
    method: 'GET',
    route: '/:id',
    handlers: [
      specializationSingle
    ]
  },
  {
    method: 'PUT',
    route: '/:id',
    handlers: [
      specializationUpdate
    ]
  },
  {
    method: 'DELETE',
    route: '/:id',
    handlers: [
      specializationDelete
    ]
  },
  {
    method: 'POST',
    route: '/',
    handlers: [
      specializationCreate
    ]
  }
];
