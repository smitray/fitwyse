import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

const specializationSchema = new mongoose.Schema({
  special: String
});

specializationSchema.plugin(uniqueValidator);
specializationSchema.plugin(timestamp);

const specializationModel = mongoose.model('specializationModel', specializationSchema);

export default specializationModel;
