import { storyboardCrud } from './';


let storyboards;
let storyboard;
let storyboardNew;

const storyboardAll = async (ctx) => {
  try {
    storyboards = await storyboardCrud.get();
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        storyboards
      },
      message: 'storyboards fetched successfully'
    };
  }
};

const storyboardSingle = async (ctx) => {
  try {
    storyboard = await storyboardCrud.single({
      qr: {
        _id: ctx.params.id
      }
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        storyboard
      },
      message: 'storyboard fetched successfully'
    };
  }
};

const storyboardCreate = async (ctx) => {
  try {
    storyboardNew = await storyboardCrud.create(ctx.request.body);
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        storyboard: storyboardNew
      },
      message: 'storyboard created successfully'
    };
  }
};

const storyboardUpdate = async (ctx) => {
  try {
    storyboard = await storyboardCrud.put({
      params: {
        qr: {
          _id: ctx.params.id
        }
      },
      body: ctx.request.body
    });
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        storyboard
      },
      message: 'storyboard updated successfully'
    };
  }
};

const storyboardDelete = async (ctx) => {
  try {
    storyboard = await storyboardCrud.delete({
      params: {
        qr: {
          _id: ctx.params.id
        }
      }
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        storyboard
      },
      message: 'storyboard deleted successfully'
    };
  }
};

export {
  storyboardAll,
  storyboardSingle,
  storyboardCreate,
  storyboardUpdate,
  storyboardDelete
};
