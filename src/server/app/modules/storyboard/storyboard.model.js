import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

const storyboardSchema = new mongoose.Schema({
  content: {
    type: String
  }
});

storyboardSchema.plugin(uniqueValidator);
storyboardSchema.plugin(timestamp);

const storyboardModel = mongoose.model('storyboardModel', storyboardSchema);

export default storyboardModel;
