export * as storyboardRouteProps from './router';
export {
  storyboardAll,
  storyboardSingle,
  storyboardCreate,
  storyboardUpdate,
  storyboardDelete
} from './controller';
export { default as storyboardModel } from './storyboard.model';
export { default as storyboardCrud } from './storyboard.service';

