import { Crud } from '@utl';

import storyboardModel from './storyboard.model';

class Servicestoryboard extends Crud {
  constructor(model) {
    super(model);
    this.model = model;
  }
}

const storyboardCrud = new Servicestoryboard(storyboardModel);
export default storyboardCrud;
