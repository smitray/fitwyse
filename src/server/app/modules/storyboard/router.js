// import { isAuthenticated } from '@mid';

import {
  storyboardAll,
  storyboardSingle,
  storyboardCreate,
  storyboardUpdate,
  storyboardDelete
} from './';

export const baseUrl = '/api/storyboard';

export const routes = [
  {
    method: 'GET',
    route: '/',
    handlers: [
      storyboardAll
    ]
  },
  {
    method: 'GET',
    route: '/:id',
    handlers: [
      storyboardSingle
    ]
  },
  {
    method: 'PUT',
    route: '/:id',
    handlers: [
      storyboardUpdate
    ]
  },
  {
    method: 'DELETE',
    route: '/:id',
    handlers: [
      storyboardDelete
    ]
  },
  {
    method: 'POST',
    route: '/',
    handlers: [
      storyboardCreate
    ]
  }
];
