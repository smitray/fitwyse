import { Crud } from '@utl';

import packageModel from './package.model';

class Servicepackage extends Crud {
  constructor(model) {
    super(model);
    this.model = model;
  }
}

const packageCrud = new Servicepackage(packageModel);
export default packageCrud;
