import { packageCrud } from './';


let packages;
let pkg;
let packageNew;

/**
@api {get} /api/package Get all package
@apiName Get all package
@apiGroup Package
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    packages: [Object]
  },
  message: package details fetched successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
 */

const packageAll = async (ctx) => {
  try {
    packages = await packageCrud.get();
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        packages
      },
      message: 'packages fetched successfully'
    };
  }
};


/**
@api {get} /api/package/:id Get single package
@apiName Get single package
@apiGroup Package
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    package: [Object]
  },
  message: package details fetched successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Email or Username exists
  HTTP/1.1 404 No record found
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
 */

const packageSingle = async (ctx) => {
  try {
    pkg = await packageCrud.single({
      qr: {
        _id: ctx.params.id
      }
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        pkg
      },
      message: 'package fetched successfully'
    };
  }
};

/**
@api {post} /api/package Create package
@apiName Create package
@apiGroup Package
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiParam {String} pkgName Package name
@apiParam {Number} cost Package cost
@apiParam {Number} duration Package duration in months
@apiParam {Date} validityFrom Package validity from
@apiParam {Date} validityTo Package validity to
@apiParam {String} activities Package activity
@apiParam {Number} maxDiscount Package maximum discount
@apiParam {Number} percentage Package percentage
@apiParam {Boolean} freezable Package freezable
@apiParam {Boolean} transferrable Package transferrable
@apiParam {String} desc Package description
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    package: [Object]
  },
  message: package created successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const packageCreate = async (ctx) => {
  try {
    packageNew = await packageCrud.create(ctx.request.body);
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        package: packageNew
      },
      message: 'package created successfully'
    };
  }
};

/**
@api {put} /api/package/:id Update package
@apiName Update package
@apiGroup Package
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiParam {String} ExampleFieldName Example details
@apiParamExample {json} Input
{
  ExampleFieldName: Example Value
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    package: [Object]
  },
  message: package updated successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const packageUpdate = async (ctx) => {
  try {
    pkg = await packageCrud.put({
      params: {
        qr: {
          _id: ctx.params.id
        }
      },
      body: ctx.request.body
    });
  } catch (e) {
    ctx.throw(422, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        pkg
      },
      message: 'package updated successfully'
    };
  }
};

/**
@api {delete} /api/package/:id Delete package
@apiName Delete package
@apiGroup Package
@apiHeader {String} Authorization JWT
@apiHeaderExample Header example
{
  Authorization: Bearer JWT
}
@apiSuccessExample {json} Success
{
  success: 1,
  data: {
    package: [Object]
  },
  message: package deleted successfully
}
@apiErrorExample {json} Server error
  HTTP/1.1 500 Internal Server Error
@apiErrorExample {json} Wrong credentials
  HTTP/1.1 401 Not authorized
@apiErrorExample {json} Wrong form key
  HTTP/1.1 422 Unprocessable entity
*/

const packageDelete = async (ctx) => {
  try {
    pkg = await packageCrud.delete({
      params: {
        qr: {
          _id: ctx.params.id
        }
      }
    });
  } catch (e) {
    ctx.throw(404, {
      success: 0,
      message: e.message
    });
  } finally {
    ctx.body = {
      success: 1,
      data: {
        pkg
      },
      message: 'package deleted successfully'
    };
  }
};

export {
  packageAll,
  packageSingle,
  packageCreate,
  packageUpdate,
  packageDelete
};
