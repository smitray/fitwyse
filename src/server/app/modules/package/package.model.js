import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';
import timestamp from 'mongoose-timestamp';

const packageSchema = new mongoose.Schema({
  pkgName: String,
  cost: Number,
  duration: Number,
  validityFrom: Date,
  validityTo: Date,
  activities: String,
  maxDiscount: Number,
  percentage: Number,
  freezable: Boolean,
  transferrable: Boolean,
  desc: String
});

packageSchema.plugin(uniqueValidator);
packageSchema.plugin(timestamp);

const packageModel = mongoose.model('packageModel', packageSchema);

export default packageModel;
